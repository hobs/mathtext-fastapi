---
title: Mathtext Fastapi
emoji: 🐨
colorFrom: blue
colorTo: red
sdk: docker
pinned: false
license: agpl-3.0
---

Check out the configuration reference at https://huggingface.co/docs/hub/spaces-config-reference
